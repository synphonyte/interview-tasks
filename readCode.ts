function whatDoesThisDo(path, key) {
  if (!path) {
    return key;
  }

  let lowerKey = key.toLowerCase();
  for (const c of path.split('.')) {
    if (lowerKey.startsWith(c.toLowerCase())) {
      lowerKey = lowerKey.substr(c.length);
    }
  }
  const idx = key.length - lowerKey.length;
  return key.substr(idx, 1).toLowerCase() + key.substr(idx + 1);
}
