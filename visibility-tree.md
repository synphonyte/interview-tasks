# Visibility Tree

Let's assume we have a number of elements in a tree-like structure (like folders and subfolders on a hard drive).
That means every element can have children which in turn can have children again. It could look like in the following picture.

![](tree.png)

As you can see next to every element there is a small eye-icon. Clicking on that icon controls the visibility of
each element and its children. All the other icons are not important for this task.

Internally there are actually two visibility states for every element, one in a so called *store* which contains
the underlying "base truth" of each element's own state and one so called *tree* state which represents if an
element is actually visible or not.

The tree state is actually not a state itself but is derived from the store state. **Whenever the store state changes the
tree state is updated as well.**

When a parent becomes invisible then all of it's children and deeper descendants also inherit this invisibility in
their tree states although their very own store state might be set to visible. That means **an element is only visible
if and only if it's own store state says it's visible and all of it's ancestors are visible as well**.

The tree state is the one that is shown through the eye icons. In the above picture every icon is visible.
An invisible element would have a grayed out eye icon like this:

![](tree_invisible.png)

When someone clicks on an eye icon in the tree he switches only the stores state for that item. But that will
immediately update its tree state and that of it's descendants.

There is one exception, though, when a click will affect more than just the items own store state.
When the user clicks on a child with an invisible
parent the user still expects to find that clicked child visible afterwards. This, as described, can only happen if
itself and all of its ancestors are visible. Thus we set the store state of the clicked-on child to visible
as well as that of its parent and grandparent and so on.

To illustrate all these principles we use this example hierarchy:

```
P
+- A
+- B
```

Now let's annotate these three objects with their visibility in the store as `vs` and the visibility that is
in the tree as `vt`.

We use upper case letters for visible and lower case for invisible.

For example if `A` is set to visible in the store but the tree shows invisible we write `A__VS__vt`.

So as our initial state we assume this:

```
P__VS__VT
+- A__VS__VT
+- B__vs__vt
```

That means everything is visible except B which is set to invisible and of course also shows that in the tree.

Now let's click on the eye icon of `P`.

As a result we get this:

```
P__vs__vt
+- A__VS__vt
+- B__vs__vt
```

In the store only the state of `P` was changed and `A` and `B` remain untouched.
Just their tree visibility `vt` has changed.

Next we click on the eye icon of `P` again:

```
P__VS__VT
+- A__VS__VT
+- B__vs__vt
```

We return to our initial state. Note that `B` remains invisible.

For the next example we set `P` to invisible one more time, so we have again:

```
P__vs__vt
+- A__VS__vt
+- B__vs__vt
```

Now let's click on the eye icon of `B`:

```
P__VS__VT
+- A__VS__VT
+- B__VS__VT
```

Now `B`'s state was set to visible in the store and so where all of it's ancestors
because otherwise even if it became visible in the store it's tree state would
still have been invisible.

What would have happened if we had clicked on `A`'s eye instead of on `B`'s?

```
P__VS__VT
+- A__VS__VT
+- B__vs__vt
```

Basically the same thing but of course `B` would have remained unchanged.

## Task

Your task is now to apply these described principles to the following examples.

1. Complete the following state chart

    ```
    P__VS__VT
    +- A__vs__vt
       +- X__VS__??
       +- Y__vs__??
    +- B__??__vt
    ```

2. What would change if we clicked on the eye icon of `P`? (Update the state of task 1.)

3. Assume this state:
    ```
    P__vs__vt
    +- A__vs__vt
       +- X__VS__vt
       +- Y__vs__vt
    +- B__VS__vt
    ```

    How does the state look like after the eye icon of `X` has been clicked?

4. Correct the errors in this state. Assume that the store state is correct.
    ```
    P__VS__VT
    +- A__VS__vt
       +- X__VS__VT
       +- Y__vs__vt
          +- U__VS__vt
    +- B__vs__VT
    ```
